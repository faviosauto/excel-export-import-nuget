﻿using System.ComponentModel.DataAnnotations;

namespace ExcelExportPackage.Models
{
    public class OrderDetail
    {
        public int Id { get; set; }

        [Required]
        public VideoGame VideoGame { get; set; }

        public Order Order { get; set; }

        [Required]
        public int Quantity { get; set; }

        public int UnitaryPrice { get; set; }

        public int Subtotal { get; set; }
    }
}
