﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExcelExportPackage.Models
{
    public class Order
    {
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        [Required]
        public int TotalAmount { get; set; }

        public IEnumerable<OrderDetail> OrderDetails { get; set; }
    }
}
