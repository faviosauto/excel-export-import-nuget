﻿using ExcelDataReader;
using ExcelExportPackage.Models;
using ExcelExportPackage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;

namespace ExcelExportPackage.Services
{
    public class ExcelOrderService : IGenericService<Order>
    {


        public IXLWorkbook ExportExcel(ICollection<Order> orders)
        {
            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Orders");

            worksheet.Cell("A1").Value = "ID";
            worksheet.Cell("B1").Value = "Order Date";
            worksheet.Cell("C1").Value = "Total Amount";

            worksheet.Cell("A2").Value = orders;

            return workbook;
        }

        public List<Order> ImportExccel()
        {
            var stream = File.Open(@"D:\Documents\Trabajos\Jabusoft\Pasantias\Mes 1\Project\videogame-store-backend\VideoGame Store Backend\Ordersx.xlsx", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);

            var conf = new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            };

            var dataSet = reader.AsDataSet(conf);
            var custom = dataSet.Tables[0].Rows[0].ItemArray.GetValue(6).ToString();
            var dataTable = dataSet.Tables[0];
            var dataTableSize = dataSet.Tables[0].Rows.Count;

            List<Order> orders = new List<Order>();

            for (var i = 0; i < dataTableSize; i++)
            {
                Order order = new Order()
                {
                    OrderDate = (DateTime)dataSet.Tables[0].Rows[i].ItemArray.GetValue(1),
                    TotalAmount = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(2).ToString()),
                };

                orders.Add(order);
            }

            return orders;
        }
    }
}
