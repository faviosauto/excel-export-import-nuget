﻿using ClosedXML.Excel;
using System.Collections.Generic;

namespace ExcelExportPackage.Services.Interfaces
{
    public interface IGenericService<T>
    {
        public IXLWorkbook ExportExcel(ICollection<T> t);

        public List<T> ImportExccel();
    }
}
