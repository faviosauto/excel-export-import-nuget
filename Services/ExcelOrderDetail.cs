﻿using ExcelDataReader;
using ExcelExportPackage.Models;
using ExcelExportPackage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;

namespace ExcelExportPackage.Services
{
    public class ExcelOrderDetailService : IGenericService<OrderDetail>
    {


        public IXLWorkbook ExportExcel(ICollection<OrderDetail> orderDetails)
        {
            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Orders");

            worksheet.Cell("A1").Value = "ID";
            worksheet.Cell("B1").Value = "Quantity";
            worksheet.Cell("C1").Value = "Unitary Price";
            worksheet.Cell("D1").Value = "Subtotal";

            worksheet.Cell("A2").Value = orderDetails;

            return workbook;
        }

        public List<OrderDetail> ImportExccel()
        {
            var stream = File.Open(@"D:\Documents\Trabajos\Jabusoft\Pasantias\Mes 1\Project\videogame-store-backend\VideoGame Store Backend\Ordersx.xlsx", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);

            var conf = new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            };

            var dataSet = reader.AsDataSet(conf);
            var custom = dataSet.Tables[0].Rows[0].ItemArray.GetValue(6).ToString();
            var dataTable = dataSet.Tables[0];
            var dataTableSize = dataSet.Tables[0].Rows.Count;

            List<OrderDetail> orderDetails = new List<OrderDetail>();

            for (var i = 0; i < dataTableSize; i++)
            {
                OrderDetail orderDetail = new OrderDetail()
                {
                    Quantity = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(1).ToString()),
                    UnitaryPrice = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(2).ToString()),
                    Subtotal = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(3).ToString()),
                };

                orderDetails.Add(orderDetail);
            }

            return orderDetails;
        }
    }
}
