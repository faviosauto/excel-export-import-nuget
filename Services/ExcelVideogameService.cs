﻿using ClosedXML.Excel;
using ExcelDataReader;
using ExcelExportPackage.Models;
using ExcelExportPackage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace ExcelExportPackage.Services
{
    public class ExcelVideogameService : IGenericService<VideoGame>
    {
        public IXLWorkbook ExportExcel(ICollection<VideoGame> videogames)
        {
            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Videogames");

            worksheet.Cell("A1").Value = "ID";
            worksheet.Cell("B1").Value = "Name";
            worksheet.Cell("C1").Value = "Developer";
            worksheet.Cell("D1").Value = "Description";
            worksheet.Cell("E1").Value = "ImageUrl";
            worksheet.Cell("F1").Value = "Price";
            worksheet.Cell("G1").Value = "Stock";

            worksheet.Cell("A2").Value = videogames;

            return workbook;
        }

        public List<VideoGame> ImportExccel()
        {
            var stream = File.Open(@"D:\Documents\Trabajos\Jabusoft\Pasantias\Mes 1\Project\videogame-store-backend\VideoGame Store Backend\Videogamesx.xlsx", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);

            var conf = new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            };

            var dataSet = reader.AsDataSet(conf);
            var custom = dataSet.Tables[0].Rows[0].ItemArray.GetValue(6).ToString();
            var dataTable = dataSet.Tables[0];
            var dataTableSize = dataSet.Tables[0].Rows.Count;    

            List<VideoGame> videogames = new List<VideoGame>();

            for (var i = 0; i < dataTableSize; i++)
            {
                VideoGame videogame = new VideoGame()
                {
                    Name = dataSet.Tables[0].Rows[i].ItemArray.GetValue(1).ToString(),
                    Developer = dataSet.Tables[0].Rows[i].ItemArray.GetValue(2).ToString(),
                    Description = dataSet.Tables[0].Rows[i].ItemArray.GetValue(3).ToString(),
                    ImageUrl = dataSet.Tables[0].Rows[i].ItemArray.GetValue(4).ToString(),
                    Price = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(5).ToString()),
                    Stock = Int16.Parse(dataSet.Tables[0].Rows[i].ItemArray.GetValue(6).ToString())
                };

                videogames.Add(videogame);
            }

            return videogames;
        }
    }
}
