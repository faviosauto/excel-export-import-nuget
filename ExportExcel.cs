﻿using ExcelExportPackage.Models;
using ExcelExportPackage.Services;
using System.Collections.Generic;
using System.IO;

namespace ExcelExportPackage
{
    public class ExportExcel
    {
        ExcelVideogameService videogameService = new ExcelVideogameService();
        ExcelOrderService OrderService = new ExcelOrderService();
        ExcelOrderDetailService OrderDetailService = new ExcelOrderDetailService();

        public MemoryStream CreateVideogameExcel(ICollection<VideoGame> videogames)
        {
            var workbook = videogameService.ExportExcel(videogames);
            var memoryStream = new MemoryStream();

            workbook.SaveAs(memoryStream);
            memoryStream.Position = 0;

            return memoryStream;
        }

        public List<VideoGame> UploadVideogameExcel()
        {
            var videogames = videogameService.ImportExccel();

            return videogames;
        }

        public MemoryStream CreateOrderExel(ICollection<Order> orders)
        {
            var workbook = OrderService.ExportExcel(orders);
            var memoryStream = new MemoryStream();

            workbook.SaveAs(memoryStream);
            memoryStream.Position = 0;

            return memoryStream;
        }

        public List<Order> UploadOrderExcel()
        {
            var orders = OrderService.ImportExccel();

            return orders;
        }

        public MemoryStream CreateOrderDetailExcel(ICollection<OrderDetail> orderDetails)
        {
            var workbook = OrderDetailService.ExportExcel(orderDetails);
            var memoryStream = new MemoryStream();

            workbook.SaveAs(memoryStream);
            memoryStream.Position = 0;

            return memoryStream;
        }

        public List<OrderDetail> UploadOrderDetailExcel()
        {
            var orderDetails = OrderDetailService.ImportExccel();

            return orderDetails;
        }
    }
}
